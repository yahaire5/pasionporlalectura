﻿ #pragma strict

var audioSound : AudioSource;
var gain : AudioClip;
var SonidoFeo1 : BoxCollider;
var SonidoFeo2 : BoxCollider;
var SonidoFeo3 : BoxCollider;
var SonidoFeo4 : BoxCollider;
var SonidoFeo5 : BoxCollider;

function Update ()
{
     if ( Input.GetMouseButtonDown(0))
     {
         var hit : RaycastHit;
         var ray : Ray = Camera.main.ScreenPointToRay (Input.mousePosition);
         
         if (Physics.Raycast (ray, hit, 100.0))
         {  
			//DontDestroyOnLoad (this);
		    audioSound.PlayOneShot(gain);
		 }
	}

	if (GameObject.Find("Murcielago") == null)
	{
		SonidoFeo1.enabled = false;
	}
	if (GameObject.Find("Sombrero") == null)
	{
		SonidoFeo2.enabled = false;
	}
	if (GameObject.Find("Perfume") == null)
	{
		SonidoFeo3.enabled = false;
	}
	if (GameObject.Find("Cucaracha") == null)
	{
		SonidoFeo4.enabled = false;
	}
	if (GameObject.Find("Pedrito") == null)
	{
		SonidoFeo5.enabled = false;
	}
}