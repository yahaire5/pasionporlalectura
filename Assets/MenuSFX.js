﻿ #pragma strict

var audioSound : AudioSource;
var menuSFX : AudioClip;
 
function SFXActive()
{
	DontDestroyOnLoad (this);
    audioSound.PlayOneShot(menuSFX);
}